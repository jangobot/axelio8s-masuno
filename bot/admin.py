
from django.contrib import admin
from models import MasUno,Mensajes,Nicknames

class MasUnosAdmin(admin.ModelAdmin):
    list_display=('nick','mensaje','destino',)

admin.site.register(MasUno,MasUnosAdmin)
