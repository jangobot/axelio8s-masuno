from django.db import models

class Nicknames(models.Model):
    nick= models.CharField(max_length=20)
    class Meta:
        db_table = u'nicknames'
    def __unicode__(self):
        return self.nick

class Mensajes(models.Model):
    mensaje= models.CharField(max_length=500)
    nickname= models.ForeignKey(Nicknames)
    class Meta:
        db_table = u'mensajes'
    def __unicode__(self):
        return "%s: %s"%(self.nickname.nick,self.mensaje)

class MasUno(models.Model):
    nick= models.ForeignKey(Nicknames,related_name='origen')
    mensaje= models.ForeignKey(Mensajes)
    destino= models.ForeignKey(Nicknames)
    class Meta:
        db_table = u'masuno'
    def cuantos(self,nick):
        return self.nick.get(nick=nick).length

    def __unicode__(self):
        return "%s: %s"%(self.nick.nick,self.mensaje.mensaje,)

